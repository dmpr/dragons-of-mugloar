package eu.dmpr.dragonsofmugloar

import eu.dmpr.dragonsofmugloar.Weather.Weather

/**
  * Created by Dmitri on 26.12.2016.
  */
case class Knight(name:String, attack:Int, armor: Int, agility: Int, endurance: Int)
case class Dragon(scaleThickness: Int, clawSharpness: Int, wingStrength: Int, fireBreath: Int)
case class Game(gameId: Int, knight: Option[Knight])
case class Coords(x:Float, y: Float, z: Float)
case class WeatherForecast(time: String, coords: Coords, code: Weather, message: String, varXRating: Float)

object Weather extends Enumeration {
  type Weather = Value
  val NORMAL = Value("NMR")
  val RAIN = Value("HVA")
  val DRY = Value("T E")
  val FOG = Value("FUNDEFINEDG")
  val STORM = Value("SRO")
}

case class GameResult(status: String, message: String)

trait GameApi {
  def chooseDragonWisely(game: Game, weather: WeatherForecast): Dragon
}

trait GameIO {
  def fetchGame: Option[Game]
  def fetchWeatherForecast(gameId: Int): Option[WeatherForecast]
  def respondToThreat(game:Game, dragon: Dragon): Option[GameResult]
}
