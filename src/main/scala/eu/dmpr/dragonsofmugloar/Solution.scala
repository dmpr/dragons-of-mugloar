package eu.dmpr.dragonsofmugloar

import com.typesafe.scalalogging.LazyLogging

/**
  * Created by Dmitri on 26.12.2016.
  */
object Solution extends App with GameApi with LazyLogging {


  logger.info("Go to battle!")

  val comm = new Communicator
  val game = comm.fetchGame
  val id = game match {
    case Some(g) => {
      val weather = comm.fetchWeatherForecast(g.gameId)
    }
    case None =>
  }
  logger.info(game.toString)

  override def chooseDragonWisely(game: Game, weather: WeatherForecast)  = ???

}
