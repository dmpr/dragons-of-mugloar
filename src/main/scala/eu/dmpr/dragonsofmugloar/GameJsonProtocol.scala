package eu.dmpr.dragonsofmugloar

import spray.json.{DefaultJsonProtocol, DeserializationException, JsNumber, JsObject, JsValue, RootJsonFormat}

/**
  * Created by Dmitri on 26.12.2016.
  */
trait GameJsonProtocol extends DefaultJsonProtocol {
  implicit val knightFormat = jsonFormat(Knight, "name","attack","armor","agility","endurance")
  implicit val gameFormat = jsonFormat(Game, "gameId", "knight")
  implicit val dragonFormat = jsonFormat(Dragon, "scaleThickness", "clawSharpness", "wingStrength", "fireBreath")
  implicit object DragonJsonFormat extends RootJsonFormat[Dragon] {
    def write(d: Dragon) = JsObject(
      "dragon" -> JsObject(
          "scaleThickness" -> JsNumber(d.scaleThickness),
          "clawSharpness" -> JsNumber(d.clawSharpness),
          "wingStrength"-> JsNumber(d.wingStrength),
          "fireBreath" -> JsNumber(d.fireBreath)
      )
    )
    def read(value: JsValue) = {
      value.asJsObject.getFields("scaleThickness", "clawSharpness", "wingStrength", "fireBreath") match {
        case Seq(JsNumber(scaleThickness), JsNumber(clawSharpness), JsNumber(wingStrength), JsNumber(fireBreath)) =>
          Dragon(scaleThickness.toInt, clawSharpness.toInt, wingStrength.toInt, fireBreath.toInt)
        case _ => throw new DeserializationException("Dragon expected")
      }
    }
  }
  implicit val gameStatusFormat = jsonFormat(GameResult, "status", "message")
}
