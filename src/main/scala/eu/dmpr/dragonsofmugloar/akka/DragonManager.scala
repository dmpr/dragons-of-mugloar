package eu.dmpr.dragonsofmugloar.akka

import com.typesafe.scalalogging.LazyLogging
import eu.dmpr.dragonsofmugloar.{Dragon, Knight, Weather, WeatherForecast}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Try

/**
  * Created by Dmitri on 30.12.2016.
  */
object DragonManager extends LazyLogging {

  private val SOLUTIONS = List(
    ((8, 8, 4, 0), (10, 6, 3, 1)),
    ((8, 7, 5, 0), (10, 5, 4, 1)),
    ((8, 6, 6, 0), (10, 4, 4, 2)),
    ((7, 7, 6, 0), (10, 4, 4, 2)),
    ((8, 8, 3, 1), (10, 6, 2, 2)),
    ((8, 7, 4, 1), (10, 5, 4, 1)),
    ((8, 6, 5, 1), (10, 4, 5, 1)),
    ((7, 7, 5, 1), (9, 5, 4, 2)),
    ((7, 6, 6, 1), (9, 4, 4, 3)),
    ((8, 8, 2, 2), (10, 6, 3, 1)),
    ((8, 7, 3, 2), (10, 5, 4, 1)),
    ((8, 6, 4, 2), (10, 4, 3, 3)),
    ((7, 7, 4, 2), (10, 5, 4, 1)),
    ((8, 5, 5, 2), (10, 5, 4, 1)),
    ((7, 6, 5, 2), (10, 4, 4, 2)),
    ((6, 6, 6, 2), (10, 4, 3, 3)),
    ((8, 6, 3, 3), (10, 4, 4, 2)),
    ((7, 7, 3, 3), (10, 4, 4, 2)),
    ((8, 5, 4, 3), (10, 4, 4, 2)),
    ((7, 6, 4, 3), (9, 4, 5, 2)),
    ((7, 5, 5, 3), (9, 5, 4, 2)),
    ((6, 6, 5, 3), (8, 5, 4, 3)),
    ((8, 4, 4, 4), (10, 4, 3, 3)),
    ((7, 5, 4, 4), (10, 4, 3, 3)),
    ((6, 6, 4, 4), (10, 4, 3, 3)),
    ((6, 5, 5, 4), (10, 4, 3, 3)),
    ((5, 5, 5, 5), (10, 4, 3, 3))
  );


  // armor < scaleThickness
  // attack < clawSharpness
  // agility < wingStrenght
  // endurance < fireBreath
  //
  // STORM - don't send the dragon
  // RAIN - Dragon: fireBreath useless, clawSharpening = max
  // FOG - Knight: agilty useless , ba
  // DRY - Balance means equal parameters
  // NORMAL -> select most powerfull skill of a knight and increase dragon skill
  def thinkAndChooseDragonWisely(knight: Knight, weatherType: Weather.Value):Option[Dragon] =
    weatherType match {
      case Weather.NORMAL => getDragonForNormalWeather(knight)
      case Weather.DRY => Some(Dragon(5, 5, 5, 5)) //!
      case Weather.FOG => Some(Dragon(0, 10, 10, 0)) // !
      case Weather.RAIN => Some(Dragon(5, 10, 5, 0)) // !
      case Weather.STORM => None
    }

  def mostPowerfullSkill(knight: Knight): Option[Dragon] = {
    // scaleThickness defends against attack 0
    // clawSharpeness attacks against armor 1
    // wingthStrength defends against agility 2
    // fireBreath attacks against endurance 3

    val aknight = Array[Int](knight.attack, knight.armor, knight.agility, knight.endurance)

    val sortedSkills = aknight.zipWithIndex.sortBy(_._1)
    var adragon = Array[Int](0, 0, 0, 0)

    val knightskills = for {
      (points, skill) <- sortedSkills
    } yield {
      logger.info(s"skill -> $points $skill")
      (points, skill)

    }

    val dragonskills = knightskills.sortBy(_._1).zipWithIndex
    for {
      ((p , s), i) <- dragonskills
    } yield {
      if (i > 1) {
        adragon(s) = p + 2
      } else {
        adragon(s) = p - 2
      }
    }


    Some(Dragon(adragon(0), adragon(1), adragon(2), adragon(3)))
//    Some(Dragon(skills._1, skills._2, (skills._3 - 1, (skills._4) - 1))
  }

  def getSecretDragonForNormalWeather(knight: Knight): Option[Dragon] = {
    // clawSharpeness has priority

    val aknight = (knight.attack, knight.armor, knight.agility, knight.endurance)

    val dragons = for {
      (k, d) <- SOLUTIONS
      if (k == aknight)
    } yield d

    logger.info(dragons.toString())

    if (dragons.isEmpty) {
      Some(Dragon(aknight._1, aknight._2, aknight._3, aknight._4))
    } else {
      val adragon = dragons.head
      val dragon = new Dragon(adragon._1, adragon._2, adragon._3, adragon._4)
      Some(dragon)
    }

  }

  def getDragonForNormalWeather(knight: Knight) = {


    val aknight = Array[Int](knight.attack, knight.armor, knight.agility, knight.endurance)
    var adragon = Array[Int](0, 0, 0, 0)

    val knightskills = for {
      (points, skill) <- aknight.zipWithIndex.sortBy(_._1)
    } yield {
      logger.info(s"skill -> $points $skill")
      (points, skill)
    }

    val dragonskills = knightskills.sortBy(_._1).zipWithIndex
    for {
      ((p , s), i) <- dragonskills
    } yield {
      i match {
        case 0 => adragon(s) = p - 1
        case 1 => adragon(s) = p + 1
        case 2 => adragon(s) = p - 1
        case 3 => adragon(s) = p + 1
      }
    }

    Some(Dragon(adragon(0), adragon(1), adragon(2), adragon(3)))
  }

  def pickTheDragon(knight: Knight, weather: WeatherForecast): Option[Dragon] = {
    val dragon = thinkAndChooseDragonWisely(knight, weather.code)
    dragon
  }

  def getFuture(dragon: Option[Dragon])(implicit ec: ExecutionContext): Future[Option[Dragon]] =
    Future {
      dragon
    }
}
