package eu.dmpr.dragonsofmugloar.akka

import java.io.IOException

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.{Materializer, ActorMaterializer}
import akka.stream.scaladsl.{Flow, Sink, Source}
import com.typesafe.scalalogging.LazyLogging
import eu.dmpr.dragonsofmugloar.akka.marshallers.{GameUnmarshaller, WeatherUnmarshaller}
import eu.dmpr.dragonsofmugloar._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

/**
  * Created by Dmitri on 26.12.2016.
  */
trait Core {
  implicit val system: ActorSystem
  implicit val materializer: Materializer
}

trait ActorCore extends Core {
  override implicit val system = ActorSystem("main")
  override implicit val materializer = ActorMaterializer()
}

object DragonsOfMugloarGameClient extends App
  with ActorCore
  with GameUnmarshaller
  with WeatherUnmarshaller
  with LazyLogging {

  //private val pool = Http().cachedHostConnectionPool[Int]("www.dragonsofmugloar.com", 80, 7)

  val connectionFlow: Flow[HttpRequest, HttpResponse, Future[Http.OutgoingConnection]] =
    Http().outgoingConnection("www.dragonsofmugloar.com", 80)

  def singleRequestPipeline(request: HttpRequest): Future[HttpResponse] =
    Source.single(request).via(connectionFlow).runWith(Sink.head)

  def fetchGameInfo: Future[Either[String, Game]] = {
    singleRequestPipeline(RequestBuilding.Get("/api/game")).flatMap { response =>
      response.status match {
        case OK =>
          Unmarshal(response.entity).to[Game].map(Right(_))
        case BadRequest =>
          Future.successful(Left(s"Bad game request"))
        case _ =>
          Unmarshal(response.entity)
            .to[String]
            .flatMap{ entity =>
              val error = s"Fail - ${response.status}"
              Future.failed(new IOException(error))
            }
      }
    }
  }

  def fetchWeatherReport(gameId: Int): Future[Either[String, WeatherForecast]] = {
    singleRequestPipeline(RequestBuilding.Get(s"/weather/api/report/$gameId"))
      .flatMap {
        response => response.status match {
          case OK =>
            Unmarshal(response.entity).to[WeatherForecast].map(Right(_))
          case BadRequest =>
            Future.successful(Left(s"Bad weather request"))
          case _ =>
            Unmarshal(response.entity).to[String].flatMap{ entity =>
              val error = s"Fail - ${response.status}"
              Future.failed(new IOException(error))
            }
        }
      }
  }

  def composePostRequest(gameId: Int, entity: RequestEntity) = {
    logger.info(s"$gameId, $entity")
    HttpRequest(
      method = HttpMethods.PUT,
      uri = s"/api/game/${gameId}/solution",
      entity = entity)
  }

  def sendDragonToBattle(gameId: Int, dragon: Option[Dragon]): Future[Either[String, GameResult]] =
    Marshal(dragon).to[RequestEntity].flatMap ( entity => {
      logger.info(entity.toString)
      singleRequestPipeline(composePostRequest(gameId, entity)).flatMap(
        response => response.status match {
          case OK =>
            Unmarshal(response.entity).to[GameResult].map(Right(_))
          case BadRequest =>
            Future.successful(Left(s"Bad weather request"))
          case _ =>
            Unmarshal(response.entity).to[String].flatMap{
              entity => {
                val error = s"Fail - ${response.status}"
                Future.failed(new IOException(error)) }
              }
          }
      )}
    )

  def getGameConditions = for {
    Right(game) <- fetchGameInfo
    Right(weather) <- fetchWeatherReport(game.gameId)
  } yield (game, weather)

  def battleReport = for {
      (game, weather) <- getGameConditions
      dragon <- DragonManager.getFuture(DragonManager.pickTheDragon(game.knight.get, weather))
      Right(gameresult) <- sendDragonToBattle(game.gameId, dragon)
    } yield (game, weather, dragon, gameresult)


  val start = System.currentTimeMillis()
  val res = Await.result(battleReport, 5 seconds)
  val end = System.currentTimeMillis()
  logger.info(s"Result in ${end - start} millis: ")
  logger.info(res._1.toString)
  logger.info(res._2.toString)
  logger.info(res._3.toString)
  logger.info(res._4.toString)


//  val client = new RestClient("www.dragonsofmugloar.com", 80, 10)

  system.terminate()
}
