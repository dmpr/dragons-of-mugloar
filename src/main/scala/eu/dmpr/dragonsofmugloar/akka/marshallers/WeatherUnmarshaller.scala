package eu.dmpr.dragonsofmugloar.akka.marshallers

import akka.http.scaladsl.marshallers.xml.ScalaXmlSupport
import akka.http.scaladsl.model.{HttpCharsets, HttpEntity, MediaTypes}
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshaller}
import akka.stream.Materializer
import eu.dmpr.dragonsofmugloar.{Coords, Weather, WeatherForecast}

import scala.xml._

/**
  * Created by Dmitri on 26.12.2016.
  */
trait WeatherUnmarshaller extends ScalaXmlSupport {

  def weatherForecastXmlEntityUnmarshaller(implicit mat: Materializer): FromEntityUnmarshaller[WeatherForecast] =
    Unmarshaller.byteStringUnmarshaller.forContentTypes(MediaTypes.`application/xml`).mapWithCharset { (data, charset) =>
      val input: String = if (charset == HttpCharsets.`UTF-8`) data.utf8String else data.decodeString(charset.nioCharset.name)
      val xml: Elem = XML.loadString(input)
      val report = xml \\ "report"
      val coords = new Coords(
        (report \\ "coords" \\ "x").text.toFloat,
        (report \\ "coords" \\ "y").text.toFloat,
        (report \\ "coords" \\ "z").text.toFloat)

      WeatherForecast(
        (report \\ "time").text,
        coords,
        Weather.withName((report \\ "code").text),
        (report \\ "message").text,
        (report \\ "varX-Rating").text.toFloat
      )
    }

  implicit def weatherUnmarshaller(implicit mat: Materializer): FromEntityUnmarshaller[WeatherForecast] =
    Unmarshaller.firstOf[HttpEntity, WeatherForecast](
      weatherForecastXmlEntityUnmarshaller
    )
}

