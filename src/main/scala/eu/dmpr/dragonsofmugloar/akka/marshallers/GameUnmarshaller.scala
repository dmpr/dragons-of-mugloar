package eu.dmpr.dragonsofmugloar.akka.marshallers

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.{HttpCharsets, HttpEntity, MediaTypes}
import akka.http.scaladsl.unmarshalling.{FromEntityUnmarshaller, Unmarshaller}
import akka.stream.Materializer
import eu.dmpr.dragonsofmugloar.{Game, GameJsonProtocol}
import spray.json._

/**
  * Created by p998pdi on 27.12.2016.
  */
trait GameUnmarshaller extends SprayJsonSupport with GameJsonProtocol {

  def gameJsonEntityUnmarshaller(implicit mat: Materializer): FromEntityUnmarshaller[Game] =
    Unmarshaller
      .byteStringUnmarshaller
      .forContentTypes(MediaTypes.`application/json`)
      .mapWithCharset { (data, charset) =>
          val input: String = if (charset == HttpCharsets.`UTF-8`) data.utf8String else data.decodeString(charset.nioCharset.name)
          val game = input.parseJson.convertTo[Game]
          game
      }

  implicit def gameUnmarshaller(implicit mat: Materializer): FromEntityUnmarshaller[Game] =
    Unmarshaller
      .firstOf[HttpEntity, Game](
        gameJsonEntityUnmarshaller
      )
}
