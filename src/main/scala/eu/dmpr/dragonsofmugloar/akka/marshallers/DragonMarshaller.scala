package eu.dmpr.dragonsofmugloar.akka.marshallers

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling.{Marshaller, ToEntityMarshaller}
import akka.http.scaladsl.model.{HttpEntity, _}
import eu.dmpr.dragonsofmugloar.{Dragon, GameJsonProtocol}
import spray.json._

import scala.concurrent.ExecutionContext

/**
  * Created by p998pdi on 27.12.2016.
  */
trait DragonMarshaller  extends GameJsonProtocol with SprayJsonSupport {

  implicit def dragonMarshaller(implicit ec: ExecutionContext): ToEntityMarshaller[Option[Dragon]] =
    Marshaller.oneOf (
      Marshaller.withFixedContentType(MediaTypes.`application/json`) {
        option =>
          option match {
            case Some(dragon) => {
              val dragonJson = Dragon(dragon.scaleThickness, dragon.clawSharpness, dragon.wingStrength, dragon.fireBreath).toJson.compactPrint
              HttpEntity(ContentType(MediaTypes.`application/json`), dragonJson)
            }
            case None => {
              HttpEntity(ContentType(MediaTypes.`application/json`), "{}")
            }
        }
      }
    )
}
