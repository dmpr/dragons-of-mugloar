package eu.dmpr.dragonsofmugloar

import com.typesafe.scalalogging.LazyLogging
import spray.json._

import scala.io.Source
import scala.xml._

/**
  * Created by Dmitri on 26.12.2016.
  */
class Communicator extends GameIO with GameJsonProtocol with LazyLogging {


  override def fetchGame = {
    val res = Source.fromURL("http://www.dragonsofmugloar.com/api/game").mkString
    logger.info(res)
    val game = res.asJson.convertTo[Game]
    logger.info(game.toString)

    game match {
      case Game(_, _) => Some(game)
      case _ => None
    }
  }

  override def fetchWeatherForecast(gameId: Int) = {

    val xml = XML.load("http://www.dragonsofmugloar.com/weather/api/report/" + gameId)

    val report = xml \\ "report"
    val coords = new Coords(
      (report \\ "coords" \\ "x").text.toFloat,
      (report \\ "coords" \\ "y").text.toFloat,
      (report \\ "coords" \\ "z").text.toFloat)

    val forecast = new WeatherForecast(
      (report \\ "time").text,
      coords,
      Weather.withName((report \\ "code").text),
      (report \\ "message").text,
      (report \\ "varX-Rating").text.toInt
    )

    logger.info(forecast.toString())
    logger.info(xml.toString())

    Some(forecast)
  }

  override def respondToThreat(game: Game, dragon: Dragon) = {

    None
  }
}
