Dragons of Mugloar API
========================
Create a program which:

Fetches new game from game API
- Optionally fetches weather from "third party" weather API
- Solves the game by selectively distributing 20 points on a dragon stats
- Sends the response to solve API
- Logs down results somehow
- Repeat from step 1 specified amount of times
- Task is considered acceptable when the application is reliably able to achieve battle success ratio above 60% (higher the better). Once you are done:

Upload your accomplishment to version control
Send the link to cv@bigbank.ee
Wait for us to get back to you.
Start battle

GET http://www.dragonsofmugloar.com/api/game
Will return a game in JSON format

```json
{
    "gameId":483159,
    "knight": {
        "name": "Sir. Russell Jones of Alberta",
        "attack": 2,
        "armor": 7,
        "agility": 3,
        "endurance": 8
    }
}
```
Solve battle

PUT http://www.dragonsofmugloar.com/api/game/{gameid}/solution
PUT a message with an optional "Dragon" in the object to the specified URL. You must spread 20 points between all four stats.

```json
{
    "dragon": {
        "scaleThickness": 10,
        "clawSharpness": 5,
        "wingStrength": 4,
        "fireBreath": 1
    }
}
```
You will receive a message stating whether battle was successful or not, and if not then why not.

Contact

jaan.pullerits@bigbank.ee or Skype for help

nele.sergejeva@bigbank.ee for more dragons

cv@bigbank.ee for a job